## Requirements

1. Android project on git repository
    - github
    - gitlab
    - bitbucket
    - example  
      [https://bitbucket.org/examples_/android-hello-world/src/master/](https://bitbucket.org/examples_/android-hello-world/src/master/)
 
2. account on https://www.bitrise.io/
    - my referral - [https://app.bitrise.io/referral/844c7db61dd06271](https://app.bitrise.io/referral/844c7db61dd06271)

## bitrise-android walkthrough
1. add app
  ![이미지 이름](public/images/00-add-app.png)
1. 계정을 선택합니다.
    빌드 내용을 공개하기 원할 경우에만 public 선택
  ![이미지 이름](public/images/01-choose-account.png)
1. git repository 선택
    github, bitbucket, gitlab, ...
    본 가이드는 bitbucket 을 예로 진행합니다.
  ![이미지 이름](public/images/02-connect-to-a-repository.png)
1. repository access 설정
  ![image](public/images/03-setup-repository-access.png)
    ADD OWN SSH - passphrase 없는 ssh 공개키 기입
    AUTOMATIC - 계정으로 자동 연동 or 추가 repository 연결
  ![실패시 bitrise.io 전용 ssh-key 수동 등록](public/images/03-1-failed-on-auto-add-ssh-key.png)
    자동 연동 실패시, ssh-key 신규로 생성 후, 해당 repository 에 등록 필요함.
1. choose branch
  ![image](public/images/04-choose-branch.png)
1. validating repository...
    문제 없을 시 통과됨.
  ![image](public/images/05-validating-repository.png)
1. project build configuration
    detected for android
  ![image](public/images/06-project-build-configuration-detected.png)
    specify build variant here
  ![image](public/images/06-project-build-configuration-android-build-variant.png)
    참고로 bitrise.io 에서 지원하는 프로젝트 타입입니다.
  ![image](public/images/06-project-build-configuration-manual.png)
1. app icon 
  ![image](public/images/07-app-icon.png)
1. webhook
  ![image](public/images/08-webhook-setup.png)
1. 기본 설정 완료
    하단 버튼 클릭시 빌드 시작
  ![image](public/images/09-all-done.png)

### workflows (default)
1. primary - 기본적인 체크
1. deploy - 배포 과정 추가

### basic
1. workflows
1. Code Signing
1. Triggers
1. Env Vars

### advanced
1. Secrets
1. Stack
1. bitrise.yml

### bitrise service 의 장/단점
## 장점
1. 쉽게 모바일 DevOps 환경 구축
1. iOS 빌드 지원
1. workflows 편집을 통한 세세한 옵션 설정 가능

## 단점
1. 느린 빌드 속도
1. 동시에 여러 빌드를 수행하려면 추가 비용 들어감 (90$/month for 2 concurrencies)
1. 무료일 경우, 허용하는 빌드시간이 10분 (workflow 편집을 통해 시간 소요 긴 부분들 빼기 가능)

## pricing
https://www.bitrise.io/pricing/teams
